<?php

/**
 * Description.
 * @copyright Copyright (c) Sigma Software
 * @package   phpPatterns
 * @author    Alexey Petrenko <alex.petrenko@sigmaukraine.com>
 */
class Singleton
{
    protected static $_instance;


    private function __clone()
    {

    }

    private function __construct()
    {

    }

    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function setConnection()
    {
        return 'connection set';
    }
}