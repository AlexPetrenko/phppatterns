<?php
/**
 * strategies.
 * @copyright Copyright (c) Sigma Software
 * @package   phpPatterns
 * @author    Alexey Petrenko <alex.petrenko@sigmaukraine.com>
 */
class EvenStrategy
{
    public function filter($i)
    {
        return (!($i % 2)) ? print($i . ' ') : '';
    }
}

class OddStrategy
{
    public function filter($i)
    {
        return ($i % 2) ? print($i . ' ') : '';
    }
}

class NumberFilter
{

    private $_strategy;

    public function __construct($strategy)
    {
        $this->_strategy = $strategy;
    }

    public function filter($i)
    {
        return $this->_strategy->filter($i);
    }
}