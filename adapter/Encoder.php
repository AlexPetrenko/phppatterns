<?php

interface IEncoder
{
    public function encode($password);
    public function check($password);
}

class MD5Encoder implements IEncoder
{
    private $_hash;
    public function encode($password)
    {
        $this->_hash = md5($password);
    }
    public function check($password)
    {
        return (md5($password) === $this->_hash) ? 1 : 0;
    }
}

class SHA1Encoder implements IEncoder
{
    private $_hash;
    public function encode($password)
    {
        $this->_hash = sha1($password);
    }
    public function check($password)
    {
        return (sha1($password) === $this->_hash) ? 1 : 0;
    }
}