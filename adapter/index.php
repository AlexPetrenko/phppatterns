<?php

include 'Encoder.php';


class Password
{
    private $password;
    private $encoder;

    public function __construct($encoder)
    {
        $this->encoder = $encoder;
    }

    public function storePassword($password)
    {
        $this->password = $this->encoder->encode($password);
    }

    public function checkHash($password)
    {
        return $this->encoder->check($password);
    }
}
$encoder = new Password(new MD5Encoder());

$password = '123456';
echo $password . '<br>';

$encoder->storePassword($password);

echo $encoder->checkHash($password) . '<br>';

echo $encoder->checkHash('sadd') . '<br>';


$encoder = new Password(new SHA1Encoder());

$password = '123456';
echo $password . '<br>';

$encoder->storePassword($password);

echo $encoder->checkHash($password) . '<br>';

echo $encoder->checkHash('sadd') . '<br>';
