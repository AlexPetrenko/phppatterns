<?php

/**
 * Decorator.
 * @copyright Copyright (c) Sigma Software
 * @package   phpPatterns
 * @author    Alexey Petrenko <alex.petrenko@sigmaukraine.com>
 */
class Decorator
{
    public function h1($text)
    {
        return '<h1>' . $text . '</h1>';
    }
    public function h2($text)
    {
        return '<h2>' . $text . '</h2>';
    }

}