<?php

    include 'controller/DefaultController.php';
    include 'controller/GreetingController.php';
    include 'model/greeting.php';


    $model = (isset($_GET['m'])) ? $_GET['m'] : '';
    $action = (isset($_GET['a'])) ? $_GET['a'] : 'index';

    switch($model) {
        case 'greeting' :
            $controller = new GreetingController();
            break;
        default :
            $controller = new DefaultController();
    }

    $controller->run($action);


