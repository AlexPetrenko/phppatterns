<?php

class GreetingController extends DefaultController
{
    private $_model;

    public function __construct()
    {
        $this->_model = new Greeting();
    }

    public function hello()
    {
        $message = $this->_model->hello();

        include 'view/message.php';
    }

    public function goodbye()
    {
        $message = $this->_model->goodbye();

        include 'view/message.php';
    }

}