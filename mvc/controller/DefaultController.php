<?php

/**
 * Description.
 * @copyright Copyright (c) Sigma Software
 * @package   phpPatterns
 * @author    Alexey Petrenko <alex.petrenko@sigmaukraine.com>
 */
class DefaultController
{

    public function run($action = 'index', $id = 0)
    {
        if (!method_exists($this, $action)) {
            $action = 'index';
        }

        return $this->$action($id);
    }

    public function index()
    {
        $message = 'Home';
        include 'view/default.php';
    }
}